Nama : Aliif Arief Maulana
<br>
NIM : 21/479029/SV/19418
<br>
MK : Praktikum Struktur Data PL1AA
<br>
# Binary Search Tree Traversal Method

terdiri dari beberapa [Node](/My_Solution/Node.py) yang disusun menjadi sebuah [Tree](/My_Solution/Tree.py)

## Pre-Order

cetak ➡️ kiri (rekursif) ➡️ kanan (rekursif)

```python
def preorder(start_node, visit_order):
    if start_node:
        visit_order = visit_order + start_node.getValue() + " "
        if start_node.has_left_child():
            visit_order = preorder(start_node.getLeft(), visit_order)
        if start_node.has_right_child():
            visit_order = preorder(start_node.getRight(), visit_order)
    return visit_order
```

## In-Order

kiri (rekursif) ➡️ cetak ➡️ kanan (rekursif)

```python
def inorder(start_node, visit_order):
    if start_node:
        if start_node.has_left_child():
            visit_order = inorder(start_node.getLeft(), visit_order)
        visit_order = visit_order + start_node.getValue() + " "
        if start_node.has_right_child():
            visit_order = preorder(start_node.getRight(), visit_order)
    return visit_order
```

## Post-Order

kiri (rekursif) ➡️ kanan (rekursif) ➡️ cetak

```python
def postorder(start_node, visit_order):
    if start_node:
        if start_node.has_left_child():
            visit_order = postorder(start_node.getLeft(), visit_order)
        if start_node.has_right_child():
            visit_order = postorder(start_node.getRight(), visit_order)
        visit_order = visit_order + start_node.getValue() + " "
    return visit_order
```