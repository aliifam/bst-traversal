from Node import Node
from Tree import Tree

mytree = Tree(Node("Aliif"))  #buat objek tree isi denga satu node yang menjadi root dengan value string ALiif
mytree.getRoot().setLeft(Node("Arief"))  #set child root kiri mytree dengan value string Arief
mytree.getRoot().setRight(Node("Maulana")) # set child root kanan mytree dengan value string Maulana
#mytree.getRoot().getLeft().setLeft(Node("Dates"))

def preorder(start_node, visit_order): #fungsi preorder traversal cetak => kri => kanan, dengan parameter root sebuah tree
    if start_node: #logika cek apakah start_node yaitu root tree apakah kosong bila sudah ada rootnya maka jalankan fungsinya
        visit_order = visit_order + start_node.getValue() + " " #paraameter visit order tambahkan dengan value start nodenya
        if start_node.has_left_child(): #bila start node memiliki child kiri maka rekursi ke arah kiri
            visit_order = preorder(start_node.getLeft(), visit_order) #ganti parameter dengan child kirinya lalu rekursi terus hingga mentok seacara tidak langsung fungsi ini akan menambah visit order juga
        if start_node.has_right_child(): #bila start node memiliki child kanan maka rekursi ke arah kanan dengan diletakkannya di if terakhir maka tiap iterasi rekursi juga akan mengecek apakah ada node kiri
            visit_order = preorder(start_node.getRight(), visit_order)  
    return visit_order #ketika semua iterasi rekursif sudah dilakukan dan mentor dengan persyaratannya maka kembalian nilai visit ordernya

def inorder(start_node, visit_order): #fungsi inorder traversal kiri => cetak => kanan, dengan parameter root sebuah tree
    if start_node: #logika cek apakah start_node yaitu root tree apakah kosong bila sudah ada rootnya maka jalankan fungsinya
        if start_node.has_left_child(): #cek apakah root punya child kiri bila punya maka ke kiri dulu
            visit_order = inorder(start_node.getLeft(), visit_order) #rekursi ke kiri sampai mentok yaitu ketika start node tidak memiliki child kiri
        visit_order = visit_order + start_node.getValue() + " "  #ketika sudah mentok maka ambil value start node tambahkan dengan visit order 
        if start_node.has_right_child(): #cek apakah start node punya child kanan namun pastikan juga child kirinya sudah dicetak atau tidak punya child kiri
            visit_order = inorder(start_node.getRight(), visit_order) #rekursi ke kanan sampai mentok
    return visit_order #ketika semua semua sudah dikunjungi maka blok return baru dijalankan yang akan mengembalikan visit order yang sudah ditambahkan dengan value2 node yang sudah dikunjungi

def postorder(start_node, visit_order): #fungsi postorder traversal kiri => kanan => cetak, dengan parameter root sebuah tree
    if start_node: #logika cek apakah start_node yaitu root tree apakah kosong bila sudah ada rootnya maka jalankan fungsinya
        if start_node.has_left_child(): #cek apakah root punya child kiri bila punya maka ke kiri dulu
            visit_order = postorder(start_node.getLeft(), visit_order) #kekiri sampai mentok dengan iterasi secara rekursi 
        if start_node.has_right_child(): #ketika ke kiri sudah mentok maka cek kanannya bila masih memiliki child di kanan maka ke kanan dulu di iterasi secara rekursi
            visit_order = postorder(start_node.getRight(), visit_order) #Iterasi ke kanan secara rekursi agar juga sekalian cek ketersedian child kiri untuk setiap root baru yang dikunjungi
        visit_order = visit_order + start_node.getValue() + " " #ketika sudah start_node sudah tidak memiliki child di kanan maupun kirinya maka tambahkan value order dengan value start_node nya
    return visit_order #ketika semua node sudah dikumjungi maka kembalikan nilai visit order yang sudah ditambah dengan value setiap node dalam tree

print(preorder(mytree.getRoot(), "")) #jalankan fungsi preorder
print(inorder(mytree.getRoot(), "")) #jalankan fungsi inorder
print(postorder(mytree.getRoot(), "")) #jalankan fungsi postorder

"""
test case
----------
root tree = Aliif
child left = Arief
child right = Maulana

output =
preorder: Aliif Arief Maulana 
inorder: Arief Aliif Maulana
postorder: Arief Maulana Aliif
"""
