class Node:
    def __init__(self, value=None):
        self.value = value
        self.right = None
        self.left = None
    
    def setValue(self, value):
        self.value = value
    
    def getValue(self):
        return self.value
    
    def setRight(self, node):
        self.right = node
    
    def getRight(self):
        return self.right
    
    def setLeft(self, node):
        self.left = node
    
    def getLeft(self):
        return self.left
    
    def has_left_child(self):
        return self.getLeft() != None
    
    def has_right_child(self):
        return self.getRight() != None
    
    def display(self):
        pass

