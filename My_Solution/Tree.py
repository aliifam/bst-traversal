from Node import Node

class Tree: #manual
    def __init__(self, node):
        self.root = node
    
    def getRoot(self):
        return self.root

"""
class Tree: #automatic biasanya pake ini
    def __init__(self, value):
        self.root = Node(value)
    
    def getRoot(self):
        return self.root
"""