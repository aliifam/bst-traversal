class Node:
    def __init__(self, data): # class Node untuk objek setiap simpul pada node tree
        self.data = data
        self.leftChild = None
        self.rightChild = None

def insertNewNode(root, data): #method untuk otomatisasi penambahan node pada BST
    if root is None:           # logika untuk cek apakah data dalam root kosong bila ia maka masukkan data node
        root=Node(data)        # karena tree masih kosong maka buat objek node dan jadikan root
        return root
    
    if data < root.data:       #logika bandingkan apakah data yang ingin  dimasukkan lebih kecil dari root bila ia maka akan menjadi leftchild
        root.leftChild = insertNewNode(root.leftChild, data) #lakukan rekursif ke leftchild
    else:
        root.rightChild = insertNewNode(root.rightChild, data) #bila data yang ingin dimasukkan lebih besar dari root maka jadikan rightchild dan lakukan rekursif
    return root

def preorder(root): #preorder yaitu cetak => kiri => kanan
    if root==None:  #cek dulu apakah root nya none bila kiri dan kanan sudah none ia maka kembalikan ini base contion dalam rekursif agar tidak infinity
        return
    print(root.data, end=", ") #selama root ada datanya maka print langsung datanya

    preorder(root.leftChild) #agar traversal bergeser ke kiri sampai data kiri habis gunakan rekursi dengan parameter leftChild 

    preorder(root.rightChild) #sampai ketika ke kiri sudah tidak bisa maka cek kanan childnya dengan rekursi juga dengan prarameter root.rightChild

def inorder(root): #inorder yaitu kiri => cetak => kanan
    if root==None: #cek dulu apakah root nya none bila kiri dan kanan sudah none ia maka kembalikan ini base condition dalam rekursif agar tidak infinity
        return
    inorder(root.leftChild) #cek apakah root masih punya leftchild bila ada maka rekursi terus sampai ujung root yang tidak memiliki left child

    print(root.data, end=", ") #karena root sudah gak ada leftchildnya maka print data node simpulnya lalu rekursi ke atas 

    inorder(root.rightChild) #bila ada root child kanan maka  maka ke kanan namun sebelumnya root parent sudah dicetak dahulu

def postorder(root): #postorder yaitu kiri => kanan => cetak
    if root==None: #cek dulu apakah root nya none bila kiri dan kanan sudah none ia maka kembalikan ini base condition dalam rekursif agar tidak infinity
        return
    postorder(root.leftChild) #cek bila root masih memiliki leftchild rekursif terus dengan parameter root.child

    postorder(root.rightChild) #bila left child sudah habis maka cek rightChildnya lalu rekursi terus sampai benar - benar sudah leaf tree

    print(root.data, end=", ") #sampai akhirnya menyentuh bagian leaf (node pada tree yang paling bawah dengan derajat 0) pada tree maka outputkan data nodenya.

def main():
    akar = Node("Aliif")        #buat objek akar isis dengan satu data yang akab nebjadi root pada tree yaitu nama depan Aliif
    data = ["Arief", "Maulana"] #list data yang akan diinsert ke dalam tree
    for i in data:              #insert data ke dalam tree dengan for each
        insertNewNode(akar, i) #gunakan method insertNewNode dengan parameter akar yang sudah memiliki data root Aliif

    print("----------------------------")

    preorder(akar) #lakukan fungsi preorder pada tree akar
    print()
    inorder(akar) #lakukan fungsi inorder pada tree akar
    print()
    postorder(akar) #lakukan fungsi postorder pada tree akar


main()

"""
test case
----------
input = Aliif Arief Maulana

perlu diingat ini insert new node dengan otomatis berarti membandingkan langsung string karena itu "Aliif" < "Arief" < "Maulana"
sesuai dengan besaran abjadnya.

output =

preorder: Aliif, Arief, Maulana, 
inorder: Aliif, Arief, Maulana,
postorder: Maulana, Arief, Aliif,
"""